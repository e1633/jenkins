package com.formation.epsi.java.superherogestion;

import com.formation.epsi.java.superherogestion.model.Power;
import com.formation.epsi.java.superherogestion.model.SuperHero;
import com.formation.epsi.java.superherogestion.model.SuperHeroHasPower;
import com.formation.epsi.java.superherogestion.repositories.PowerRepository;
import com.formation.epsi.java.superherogestion.repositories.SuperHasPowerRepository;
import com.formation.epsi.java.superherogestion.repositories.SuperHeroRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class SuperherogestionApplication {

    private final PowerRepository powerRepository;
    private final SuperHeroRepository superHeroRepository;
    private final SuperHasPowerRepository superHasPowerRepository;

    SuperherogestionApplication(PowerRepository powerRepository,
            SuperHeroRepository superHeroRepository, SuperHasPowerRepository superHasPowerRepository) {
        this.powerRepository=powerRepository;
        this.superHeroRepository=superHeroRepository;
        this.superHasPowerRepository=superHasPowerRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(SuperherogestionApplication.class, args);
    }

   /* @PostConstruct
    public void initData() {
        Power power = new Power();
        power.setPowerName("Vol");

        SuperHero superHero = new SuperHero();
        superHero.setSuperHeroName("Tornade");

        Power createdPower = powerRepository.save(power);
          System.out.println(createdPower.getId());

        SuperHero createdSuper = superHeroRepository.save(superHero);

        SuperHeroHasPower superHeroHasPower = new SuperHeroHasPower(superHero,power);
        SuperHeroHasPower heroHasPower = superHasPowerRepository.save(superHeroHasPower);




    }*/

}


