package com.formation.epsi.java.superherogestion.repositories;

import com.formation.epsi.java.superherogestion.model.SuperHeroHasPower;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SuperHasPowerRepository extends JpaRepository<SuperHeroHasPower, Long> {
}
