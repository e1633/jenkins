package com.formation.epsi.java.superherogestion.model;


import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table
public class Vilain implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Ce champ ne peut être vide")
    private String name;

    @ManyToMany(mappedBy = "vilains")
    private Set<SuperHero> superheroes = new HashSet<>();

    public Vilain() {
    }

    public Vilain(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<SuperHero> getSuperheroes() {
        return superheroes;
    }

    public void setSuperheroes(Set<SuperHero> superheroes) {
        this.superheroes = superheroes;
    }

    @Override
    public String toString() {
        return "Vilain{" +
                ", name='" + name + '\'' +
                '}';
    }
}
