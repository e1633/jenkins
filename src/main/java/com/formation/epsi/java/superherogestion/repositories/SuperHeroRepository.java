package com.formation.epsi.java.superherogestion.repositories;

import com.formation.epsi.java.superherogestion.model.SuperHero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SuperHeroRepository extends JpaRepository<SuperHero, Long> {
}
