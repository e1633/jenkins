package com.formation.epsi.java.superherogestion.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table
public class SuperHero implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Transient
    private Long copainId;

    @NotEmpty(message = "Ce champ ne peut être vide")
    private String superHeroName;
    @NotEmpty(message = "Ce champ ne peut être vide")
    private String secretIdentity;

    @OneToMany(mappedBy = "superhero")
    private List<SuperHeroHasPower> powers;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "Superhero_Vilain",
            joinColumns = { @JoinColumn(name = "superhero_id") },
            inverseJoinColumns = { @JoinColumn(name = "vilain_id") }
    )
    Set<Vilain> vilains = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
            @NotFound(
            action = NotFoundAction.IGNORE)
    @JoinColumn(name = "leader_id" )
    private SuperHero leader;

    @OneToMany(mappedBy = "leader")
    private Set<SuperHero> copains = new HashSet<SuperHero>();


    public SuperHero() {
        this.powers = new ArrayList<>();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSuperHeroName() {
        return superHeroName;
    }

    public void setSuperHeroName(String superHeroName) {
        this.superHeroName = superHeroName;
    }

    public String getSecretIdentity() {
        return secretIdentity;
    }

    public void setSecretIdentity(String secretIdentity) {
        this.secretIdentity = secretIdentity;
    }

    @JsonIgnore
    public List<SuperHeroHasPower> getPowers() {
        return powers;
    }

    public void setPowers(List<SuperHeroHasPower> powers) {
        this.powers = powers;
    }

    public Set<Vilain> getVilains() {
        return vilains;
    }

    public void setVilains(Set<Vilain> vilains) {
        this.vilains = vilains;
    }

    @Override
    public String toString() {
        return "SuperHero{" +
                ", superHeroName='" + superHeroName + '\'' +
                '}';
    }
}
