package com.formation.epsi.java.superherogestion.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class SuperHeroHasPower implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "superhero_id")
    SuperHero superhero;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "power_id")
    Power power;

    public SuperHeroHasPower() {
    }

    public SuperHeroHasPower(SuperHero superhero, Power power) {
        this.superhero = superhero;
        this.power = power;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SuperHero getSuperhero() {
        return superhero;
    }

    public void setSuperhero(SuperHero superhero) {
        this.superhero = superhero;
    }

    public Power getPower() {
        return power;
    }

    public void setPower(Power power) {
        this.power = power;
    }

}
