package com.formation.epsi.java.superherogestion.repositories;

import com.formation.epsi.java.superherogestion.model.Vilain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VilainRepository  extends JpaRepository<Vilain, Long> {
}
