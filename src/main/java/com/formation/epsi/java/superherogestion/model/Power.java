package com.formation.epsi.java.superherogestion.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Power implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Ce champ ne peut être vide")
    private String powerName;

    @OneToMany(mappedBy = "power",fetch = FetchType.LAZY)
    @NotFound(
            action = NotFoundAction.IGNORE)
    List<SuperHeroHasPower> powers;

    public Power() {
        this.powers = new ArrayList<>();
    }

    public Power(Long id, String powerName, List<SuperHeroHasPower> powers) {
        this.id = id;
        this.powerName = powerName;
        this.powers = powers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPowerName() {
        return powerName;
    }

    public void setPowerName(String powerName) {
        this.powerName = powerName;
    }

    @JsonIgnore
    public List<SuperHeroHasPower> getPowers() {
        return powers;
    }

    public void setPowers(List<SuperHeroHasPower> powers) {
        this.powers = powers;
    }

    @Override
    public String toString() {
        return "Power{" +
                ", powerName='" + powerName + '\'' +
                '}';
    }
}
