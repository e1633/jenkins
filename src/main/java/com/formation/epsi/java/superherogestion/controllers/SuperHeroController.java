package com.formation.epsi.java.superherogestion.controllers;

import com.formation.epsi.java.superherogestion.model.SuperHero;
import com.formation.epsi.java.superherogestion.repositories.SuperHeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/superHeroes")
public class SuperHeroController {

    @Autowired
    private SuperHeroRepository superHeroRepository;

    public SuperHeroController(SuperHeroRepository superHeroRepository){
        this.superHeroRepository = superHeroRepository;
    }

    @GetMapping
    public List<SuperHero> findAll() {

        return superHeroRepository.findAll();
    }

    @GetMapping("/{id}")
    public SuperHero getHeroById(@PathVariable Long id) {
        return superHeroRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @PostMapping
    public ResponseEntity createSuperHero(@RequestBody SuperHero superHero) throws URISyntaxException {
        SuperHero savedSuperHero = superHeroRepository.save(superHero);
        return ResponseEntity.created(new URI("/add"+savedSuperHero.getId())).body(savedSuperHero);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateSuperHero(@PathVariable Long id, @RequestBody SuperHero superHero) {
        SuperHero currentSuperHero = superHeroRepository.findById(id).orElseThrow(RuntimeException::new);
        currentSuperHero.setSuperHeroName(superHero.getSuperHeroName());
        currentSuperHero = superHeroRepository.save(superHero);

        return ResponseEntity.ok(currentSuperHero);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteSuperHero(@PathVariable Long id) {
        superHeroRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }


}
