package com.formation.epsi.java.superherogestion.repositories;

import com.formation.epsi.java.superherogestion.model.Power;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PowerRepository extends JpaRepository<Power, Long> {
}
